#!/bin/bash

#First install the GUI
dnf install epel-release python3 python3-pip -y
dnf group install 'workstation' -y
dnf remove -y initial-setup initial-setup-gui

systemctl set-default graphical.target
systemctl isolate graphical.target

#Install epel and python
dnf install epel-release python3 python3-pip git -y

#Uncomment if you require PHP7.4
#dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

#Epel was required for php 7
dnf install php -y

# Setup VSCode
# Get the gpg key
rpm --import https://packages.microsoft.com/keys/microsoft.asc

# Create the repo
tee /etc/yum.repos.d/vscode.repo <<ADDREPO
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
ADDREPO

# Install vscode
dnf install code -y

