# Development environment
This is a standard development environment for Windows users who want to leverage the practicalities of developing on Linux.
This system is built using Vagrant, Virtualbox.

## What is included
Standard software installed in this environment:
* CentOS 8
* VSCode
* Python3.6
* Pip
* Git
* PHP 7.2

## How to use
First install Vagrant. You can get the latest version of Vagrant from this location: https://www.vagrantup.com/downloads.html
Next install VirtualBox. You can download the latest version of VirtualBox from this location: https://www.virtualbox.org/wiki/Downloads

Now for the easy bit. 
* Start a command terminal. 
* In the terminal, go to the directory where you placed these files
* Issue the following command
```
$ vagrant up
```
The first run may take some time. How long exactly depends on your computers specs.
You know should see output like thise
```
    dev01: Transaction check succeeded.
    dev01: Running transaction test
    dev01: Transaction test succeeded.
    dev01: Running transaction
    dev01:   Preparing        :                                                        1/1
    dev01:
    dev01:   Installing       : code-1.41.1-1576681965.el7.x86_64                      1/1
    dev01:
    dev01:   Running scriptlet: code-1.41.1-1576681965.el7.x86_64                      1/1
    dev01:
    dev01:   Verifying        : code-1.41.1-1576681965.el7.x86_64                      1/1
    dev01:
    dev01:
    dev01: Installed:
    dev01:   code-1.41.1-1576681965.el7.x86_64
    dev01: Complete!
```

The default password for the __vagrant__ user is _vagrant_.

## System memory
The VM is configured with 4096Mb of memory by default. This value can be modified in the [Vagrantfile](./Vagrantfile).

```
    dev01.vm.provider "virtualbox" do |vb|
      vb.gui = true
      vb.memory = "4096"
    end
```

## Posible problems
If you see output similar to the following:
```
[Not in a hypervisor partition (HVP=0) (VERR_NEM_NOT_AVAILABLE).
VT-x is disabled in the BIOS for all CPU modes (VERR_VMX_MSR_ALL_VMX_DISABLED).
```
That means you CPU isn't configured for Virtualization. Enable this in you computers BIOS.

## Usefull commands
```
$ vagrant up
```
Starts the VM.

```
$ vagrant up --provision
or
$ vagrant provision
```
(Re)provisions the VM. Using the up command also starts it.

```
$ vagrant ssh
```
SSH into the VM from the terminal.

```
$ vagrant halt
```
Stops the VM.

```
$ vagrant destroy
```
Throws all local files related to vagrant away.